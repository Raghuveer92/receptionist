package simplifii.framework.utility;

import java.util.LinkedHashMap;

public interface AppConstants {

    String DEF_REGULAR_FONT = "Roboto-Regular.ttf";
    String APP_LINK = "https://drive.google.com/file/d/0B8wKJnD6sONHeXlUbm5pOTk4dGM/view?usp=sharing";
    LinkedHashMap<Integer, String> storeCategory = new LinkedHashMap<Integer, String>();
    int TASKCODE_REGISTER = 10;
    int TASKCODE_LOGIN = 11;
    int TASKCODE_BRICKS_REGISTER = 12;
    int REQUESTCODE_GOOGLE_SIGHN_IN = 13;
    int TASKCODE_WHEELS_REGISTER = 14;
    int TASKCODE_SEARCH_BRICKS = 15;
    int TASKCODE_SEARCH_WHEELS = 16;
    int TASK_CODE_IMAGE_SLIDER = 17;
    String REDIRECT_TO = "redirectTo";

    interface REUEST_CODES {

        int DIAGNOSIS = 10;
        int FURTHER = 11;
        int ILLNESS = 12;
        int ALLERGY = 13;
        int VITALS = 15;
        int SYSTEM_REVIEW = 16;
        int REGISTER_PATIENT = 17;
        int CHANGE_PHYSICIAN = 18;
    }


    public static interface PARAMS {
        String LAT = "latitude";
        String LNG = "longitude";
    }

    public static interface VALIDATIONS {
        String EMPTY = "empty";
        String EMAIL = "email";
        String MOBILE = "mobile";
    }

    public static interface ERROR_CODES {

        public static final int UNKNOWN_ERROR = 0;
        public static final int NO_INTERNET_ERROR = 1;
        public static final int NETWORK_SLOW_ERROR = 2;
        public static final int URL_INVALID = 3;
        public static final int DEVELOPMENT_ERROR = 4;

    }

    public static interface PAGE_URL {
        String BASE_URL = "http://35.154.72.39:8080/syntagi/";
//        String BASE_URL = "http://35.154.72.39:8080/syntagi/";
        String LOGIN = BASE_URL + "new/login/users";
        String REGISTER_PATIENT = BASE_URL + "patient/create";
        String ADD_TO_QUEUE = BASE_URL + "queue/create";
        String GET_PATIENT_QUEUE = BASE_URL + "queue/list";
        String GET_PHYSICIAN = BASE_URL + "clinic/getPhysicians";
        String DELETE_QUEUE_ENTRY = BASE_URL + "/queue/delete";
        String CHANGE_PHYSICIAN = BASE_URL + "/queue/changePhysician";
        String GET_ALL_CLINIC = BASE_URL + "/clinic/get/all";
        String SEND_OTP =BASE_URL+ "sms/generateOtp";
        String UPLOAD_IMAGE = BASE_URL + "/file/image/upload";
    }

    public static interface PREF_KEYS {

        String KEY_LOGIN = "IsUserLoggedIn";
        String KEY_USERNAME = "username";
        String KEY_PASSWORD = "password";
        String ACCESS_CODE = "access";
        String APP_LINK = "appLink";


        String USER_ID = "user_id";
        String IS_LOGIN = "is_login";
        String USER_INSTANCE = "user_intance";
        String SELLER_NAME = "user_name";
        String SELLER_MOBILE = "Seller_mobile";
        String SELLER_EMAIL = "seller_email";
        String SELLER_INSTANCE = "seller Instance";
        String USER_TOKEN = "user_token";
        String USER_EMAIL = "user_email";
        String USER_NAME = "username";
        String OBJECT_ID = "object_id";
        String PAST_CURRENT_ILLNESS = "past_current_illness";
        String DIAGNOSIS = "diagnosis";
        String ALLERGY = "allergy";
        String SYSTEM_REVIEW = "system_review";
        String FURTHER_ANALIST = "ferther";
        String VITALS = "vitals";
        String MEDICINES = "medicine";
        String DICTIONARY_ADDED = "dictionary_added";
        String ADDING_SUGETIONS = "adding_suggetions";
        String DIAGNOSIS_DATA = "diagnosis_data";

        String RECEPTIONIST_DATA = "receptionist_data";
        String CLINIC_NAME = "clinic_name";
        String PATIENT_DATA = "patient_data";
    }

    public static interface BUNDLE_KEYS {
        public static final String KEY_SERIALIZABLE_OBJECT = "KEY_SERIALIZABLE_OBJECT";
        public static final String FRAGMENT_TYPE = "FRAGMENT_TYPE";
        String EXTRA_BUNDLE = "bundle";
        String NUMBER = "number";
        String SEARCH = "search";
        String QUEUE_ID = "queueId";
        String RECEPTIONIST_DATA = "receptionistData";
        String PATIENT_DATA = "patientData";
        String PHYSICIAN_ID = "physician_id";
    }

    public static interface FRAGMENT_TYPE {
        int HOME_FRAGMENT = 0;
        int TEAMS_FRAGMENT = 1;
        int ADD_TEAM_MATE = 2;
        int MY_TEAMS = 3;
        int MY_ORDERS = 4;
        int WEB_VIEW = 5;
    }

    public static interface VIEW_TYPE {
        int CARD_MY_TEAM = 0;
    }

    public interface TASK_CODES {
        int GET_CLINIC = 20;
        int GET_PATIENT_QUEUE = 21;
        int GET_PHYSICIAN = 22;
        int REGISTER_RECEPTIONIST = 30;
        int REGEISTER_PATIENT = 31;
        int FIND_PATIENT_BY_NUMBER = 32;
        int ADD_TO_QUEUE = 33;
        int DELETE_QUEUE_ENTRY = 34;
        int CHANGE_PHYSICIAN = 35;
        int FIND_RECEPTIONIST_BY_NUMBER = 36;
        int GET_AUTH_KEY = 37;
        int SEND_OTP = 38;
        int GET_ALL_CLINICS = 39;
        int UPLOAD_IMAGE = 40;
    }

    interface MEDIA_TYPES {
        String IMAGE = "img";
        String AUDIO = "audio";
        String VIDEO = "video";
    }

    interface JSON_FILES {
        String folder = "json/";
    }

    interface PatientConstants{
        String BASE = PAGE_URL.BASE_URL + "/patient";
        String FIND = BASE + "/find";
        String FIND_BY_NUMBER_AND_MATCH_PREFIX = BASE + "/findByNumberAndMatchPrefix";
    }

    interface ClinicConstants{
        String BASE = PAGE_URL.BASE_URL + "/clinic";
        String GET = BASE + "/get";
        String GET_ALL = BASE + "/get/all";
    }

    interface SmsConstants{
        String BASE = PAGE_URL.BASE_URL + "/sms";
        String GET_AUTH_KEY = BASE + "/getAuthKey";
        String SEND_OTP = "https://control.msg91.com/api/sendhttp.php";
    }

    interface ReceptionistConstants{
        String BASE = PAGE_URL.BASE_URL + "/receptionist";
        String REGISTER = BASE + "/create";
        String FIND_BY_NUMBER = BASE + "/findByNumber";
    }

}
