package com.example.my.receptionist.Model;

import android.content.Intent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dhillon on 05-10-2016.
 */

public class QueueData {

    @SerializedName("queueId")
    @Expose
    private String queueId;
    @SerializedName("clinicId")
    @Expose
    private String clinicId;
    @SerializedName("patientId")
    @Expose
    private String patientId;
    @SerializedName("physicianId")
    @Expose
    private String physicianId;
    @SerializedName("appointmentTime")
    @Expose
    private long appointmentTime;
    @SerializedName("removalTime")
    @Expose
    private long removalTime;

    /**
     * @return The queueId
     */
    public String getQueueId() {
        return queueId;
    }

    /**
     * @param queueId The queueId
     */
    public void setQueueId(String queueId) {
        this.queueId = queueId;
    }

    /**
     * @return The clinicId
     */
    public String getClinicId() {
        return clinicId;
    }

    /**
     * @param clinicId The clinicId
     */
    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    /**
     * @return The patientId
     */
    public String getPatientId() {
        return patientId;
    }

    /**
     * @param patientId The patientId
     */
    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    /**
     * @return The physicianId
     */
    public String getPhysicianId() {
        return physicianId;
    }

    /**
     * @param physicianId The physicianId
     */
    public void setPhysicianId(String physicianId) {
        this.physicianId = physicianId;
    }

    /**
     * @return The appointmentTime
     */
    public long getAppointmentTime() {
        return appointmentTime;
    }

    /**
     * @param appointmentTime The appointmentTime
     */
    public void setAppointmentTime(long appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    /**
     * @return The removalTime
     */
    public long getRemovalTime() {
        return removalTime;
    }

    /**
     * @param removalTime The removalTime
     */
    public void setRemovalTime(long removalTime) {
        this.removalTime = removalTime;
    }

}