package com.example.my.receptionist.Model.clinic;

import java.io.Serializable;

/**
 * Created by raghu on 16/1/17.
 */

public class ClinicData implements Serializable{
    String clinicId;
    String name;
    Address clinicAddress;

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getClinicAddress() {
        return clinicAddress;
    }

    public void setClinicAddress(Address clinicAddress) {
        this.clinicAddress = clinicAddress;
    }
}
