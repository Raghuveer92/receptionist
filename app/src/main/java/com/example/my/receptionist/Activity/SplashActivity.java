package com.example.my.receptionist.Activity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.my.receptionist.R;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Dhillon on 04-10-2016.
 */

public class SplashActivity extends BaseActivity {
    private ImageView iv_logo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        final Boolean isLogin = Preferences.getData(AppConstants.PREF_KEYS.IS_LOGIN, false);
        iv_logo = (ImageView) findViewById(R.id.imageView);
        Animation animation = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.fragment_slid_up);
        iv_logo.setAnimation(animation);
        animation.start();
        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if(isLogin){
                        startNextActivity(HomeActivity.class);
                        finish();
                        return;
                    }
                    startNextActivity(SplashLoginActivity.class);
                    overridePendingTransition(R.anim.fragment_slid_up, R.anim.fragment_slid_down);
                    finish();
                }
            }
        }.start();
    }
}
