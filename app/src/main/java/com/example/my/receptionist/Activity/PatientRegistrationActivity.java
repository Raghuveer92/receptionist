package com.example.my.receptionist.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.receptionist.Fragments.MediaFragment;
import com.example.my.receptionist.ImageUploadTask;
import com.example.my.receptionist.Model.CreatePatientApi;
import com.example.my.receptionist.R;
import com.example.my.receptionist.utils.FileUploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;

public class PatientRegistrationActivity extends BaseActivity {

    TextView tv_male, tv_female;
    String gender = null;
    private MediaFragment imagePicker;
    private Bitmap profileBitmap;
    private ImageView profilePic;
    private String profileImageUrl;
    private String city;
    private String clinicId;
    private String dateOfBirth;
    private Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_registration);
        calendar = Calendar.getInstance();

        initToolBar("Register Patient");
        getHomeIcon();

        tv_male = (TextView) findViewById(R.id.tv_male);
        tv_female = (TextView) findViewById(R.id.tv_female);
        profilePic = (ImageView) findViewById(R.id.iv_patient_profile_pic);

        imagePicker = new MediaFragment();
        getSupportFragmentManager().beginTransaction().add(imagePicker, "Profile Pic").commit();

        setOnClickListener(R.id.btn_reg, R.id.tv_male, R.id.tv_female, R.id.iv_patient_profile_pic, R.id.et_date_of_birth);

    }

    @Override
    public void onClick(View v) {
        unselectAll();
        switch (v.getId()) {
            case R.id.tv_male:
                setSelect(tv_male);
                break;
            case R.id.tv_female:
                setSelect(tv_female);
                break;
            case R.id.btn_reg:
                if (isValidMobile(R.id.et_mobile) && gender != null) {
                    if (profileBitmap == null) {
                        registerPatient("");
                        return;
                    }
                    showProgressDialog();
                    FileUploadTask.uploadImage(profileBitmap, new FileUploadTask.OnFileUploadListener() {
                        @Override
                        public void onSuccess(String imageUrl) {
                            profileImageUrl = imageUrl;
                            showToast("Image successfully uploaded..!");
                            registerPatient(profileImageUrl);
                        }

                        @Override
                        public void onFailed() {
                            showToast("Image upload failed..!");
                            registerPatient("");
                        }
                    }, PatientRegistrationActivity.this);
//                    ImageUploadTask imageUploadTask = ImageUploadTask.uploadImage(new ImageUploadTask.UploadListener() {
//                        @Override
//                        public void onUpload(String imageUrls) {
//                            profileImageUrl = imageUrls;
//                            hideProgressBar();
//                            registerPatient(profileImageUrl);
//                        }
//
//                        @Override
//                        public void onUploadFailed() {
//                            showToast("Image upload failed");
//                            hideProgressBar();
//                            registerPatient("");
//
//                        }
//                    }, profileBitmap);
//                    imageUploadTask.execute();
                }
                break;
            case R.id.et_date_of_birth:
                getDateOfBirth();
                break;
            case R.id.iv_patient_profile_pic:
                imagePicker.getImage(new MediaFragment.ImageListener() {
                    @Override
                    public void onGetBitMap(Bitmap bitmap) {
                        profileBitmap = bitmap;
                        profilePic.setImageBitmap(profileBitmap);
                    }
                });
        }
    }

    private void getDateOfBirth() {
        int mYear = calendar.get(Calendar.YEAR);
        final int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(year,monthOfYear,dayOfMonth);
                        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
                        dateOfBirth = simpleDateFormat.format(calendar.getTime());
                        setText(dateOfBirth,R.id.et_date_of_birth);
                        ImageView imageView= (ImageView) findViewById(R.id.iv_date);
                        imageView.setImageResource(R.drawable.ic_calendar_tap);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();
    }

    private boolean isValidMobile(int id) {
        EditText et = (EditText) findViewById(id);
        if (et.getText().toString().trim().length() != 10) {
            setError(id, "Invalid Phone Number");
            return false;
        }
        return true;
    }

    private void registerPatient(String imageUrl) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.REGISTER_PATIENT);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(CreatePatientApi.class);
        httpParamObject.setJson(getPatientData(imageUrl).toString());
        executeTask(AppConstants.TASK_CODES.REGEISTER_PATIENT, httpParamObject);
    }

    private JSONObject getPatientData(String imageUrl) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", getEtText(R.id.et_fullname));
            jsonObject.put("gender", gender);
            jsonObject.put("dob", getEtText(R.id.et_date_of_birth));
            jsonObject.put("emailId", getEtText(R.id.et_email));
            jsonObject.put("phoneNumber", getEtText(R.id.et_mobile));
            jsonObject.put("imageUrl", imageUrl);
            jsonObject.put("clinicId", clinicId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    protected String getEtText(int editTextId) {
        EditText et = (EditText) findViewById(editTextId);
        String data = et.getText().toString();
        if (TextUtils.isEmpty(data)) {
            setError(editTextId, getString(R.string.empty_error));
            return null;
        }
        return data;
    }

    private void unselectAll() {
        tv_male.setTextColor(Color.GRAY);
        tv_female.setTextColor(Color.GRAY);
    }

    private String getTilText(int id) {
        TextInputLayout viewId = (TextInputLayout) findViewById(id);
        if (null != viewId) {
            return viewId.getEditText().getText().toString();
        }
        return "";
    }

    private void setSelect(TextView id) {
        id.setTextColor(getResourceColor(R.color.orange));
        gender = id.getText().toString().toUpperCase();
    }

    protected int getHomeIcon() {
        return R.drawable.home_button;
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        if (re != null) {
            showToast(re.getMessage());
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null)
            return;
        switch (taskCode) {
            case AppConstants.TASK_CODES.REGEISTER_PATIENT:
                CreatePatientApi createPatientApi = (CreatePatientApi) response;
                if (null != response && false == createPatientApi.getError()) {
                    //showToast(createPatientApi.getMessage());
                    showToast("Patient registered successfully!");
                    Intent intent1 = getIntent();
                    Intent redirectToIntent = intent1;
                    String redirectTo = redirectToIntent.getStringExtra(AppConstants.REDIRECT_TO);
                    if (SelectPhysicianActivity.class.getSimpleName().equals(redirectTo)) {
                        Intent intent = new Intent(PatientRegistrationActivity.this, SelectPhysicianActivity.class);
                        intent1.putExtra(AppConstants.BUNDLE_KEYS.PATIENT_DATA,createPatientApi.getData());
                        intent.putExtras(intent1);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        startNextActivity(HomeActivity.class);
                        finish();
                    }
                }
                break;
        }
    }

}
