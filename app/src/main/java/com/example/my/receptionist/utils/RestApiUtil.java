package com.example.my.receptionist.utils;

import com.example.my.receptionist.Model.ClinicDetailsApi;
import com.example.my.receptionist.Model.CreateReceptionistApi;
import com.example.my.receptionist.Model.FindByNumber;
import com.example.my.receptionist.Model.FindByNumberAndMatchPrefix;
import com.example.my.receptionist.Model.GetAuthKeyApi;
import com.example.my.receptionist.Model.GetReceptionistApi;
import com.example.my.receptionist.Model.SendOtpResponse;
import com.example.my.receptionist.Model.clinic.ClinicResponse;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by robin on 10/4/16.
 */

public class RestApiUtil {

    public static HttpParamObject findPatientByPhoneNumberRequest(String phoneNumber){
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(FindByNumber.class);
        httpParamObject.setUrl(AppConstants.PatientConstants.FIND);
        httpParamObject.addParameter("number", phoneNumber);
        return httpParamObject;
    }

    public static HttpParamObject findPatientByNumberAndMatchPrefixRequest(String phoneNumber, String prefix){
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(FindByNumberAndMatchPrefix.class);
        httpParamObject.setUrl(AppConstants.PatientConstants.FIND_BY_NUMBER_AND_MATCH_PREFIX);
        httpParamObject.addParameter("number", phoneNumber);
        httpParamObject.addParameter("prefix", prefix);
        return httpParamObject;
    }

    public static HttpParamObject getAllClinicsRequest(){
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.ClinicConstants.GET_ALL);
        httpParamObject.setClassType(ClinicResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject registerReceptionistRequest(String json){
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.ReceptionistConstants.REGISTER);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(CreateReceptionistApi.class);
        httpParamObject.setJson(json);
        return httpParamObject;
    }

    public static HttpParamObject findReceptionistByNumber(String number) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.ReceptionistConstants.FIND_BY_NUMBER);
        httpParamObject.addParameter("number",number);
        httpParamObject.setClassType(GetReceptionistApi.class);
        return httpParamObject;
    }

    public static HttpParamObject generateAndSendOtp(String authKey) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SEND_OTP);
        httpParamObject.addParameter("type","phone");
        httpParamObject.addParameter("value",authKey);
        httpParamObject.setClassType(SendOtpResponse.class);
        return httpParamObject;
    }
}
