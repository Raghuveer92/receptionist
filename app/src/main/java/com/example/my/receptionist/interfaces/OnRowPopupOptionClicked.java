package com.example.my.receptionist.interfaces;

import com.example.my.receptionist.Model.PhysicianData;

public interface OnRowPopupOptionClicked{
        void removePatientQueueFromQueue(String queueId);
        void changePhysician(String queueId, String physicianDetails);
    }