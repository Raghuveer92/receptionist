package com.example.my.receptionist.Model.clinic;

import java.io.Serializable;
import java.util.List;

/**
 * Created by raghu on 16/1/17.
 */

public class PhysiciansClinicData implements Serializable {
    private ClinicData clinicData;
    private List<Integer> frequencies;
    private String timeFrom;
    private String timeTo;

    public ClinicData getClinicData() {
        return clinicData;
    }

    public void setClinicData(ClinicData clinicData) {
        this.clinicData = clinicData;
    }

    public List<Integer> getFrequencies() {
        return frequencies;
    }

    public void setFrequencies(List<Integer> frequencies) {
        this.frequencies = frequencies;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }
}
