package com.example.my.receptionist.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dhillon on 06-10-2016.
 */

public class PatientQueueData {

    @SerializedName("queueDetails")
    @Expose
    private QueueData queueDetails;
    @SerializedName("patientDetails")
    @Expose
    private PatientData patientDetails;
    @SerializedName("physicianDetails")
    @Expose
    private PhysicianData physicianDetails;

    public QueueData getQueueDetails() {
        return queueDetails;
    }

    public void setQueueDetails(QueueData queueDetails) {
        this.queueDetails = queueDetails;
    }

    public PatientData getPatientDetails() {
        return patientDetails;
    }

    public void setPatientDetails(PatientData patientDetails) {
        this.patientDetails = patientDetails;
    }

    public PhysicianData getPhysicianDetails() {
        return physicianDetails;
    }

    public void setPhysicianDetails(PhysicianData physicianDetails) {
        this.physicianDetails = physicianDetails;
    }
}