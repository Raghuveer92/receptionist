package com.example.my.receptionist.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by robin on 11/7/16.
 */

public class BaseApiResponse {

    @Expose
    @SerializedName(value = "error")
    private Boolean error;
    @Expose
    @SerializedName(value = "code")
    private String code;
    @Expose
    @SerializedName(value = "message")
    private String message;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}