package com.example.my.receptionist.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.receptionist.Fragments.AllPatientsFragments;
import com.example.my.receptionist.Fragments.DoctorFragment;
import com.example.my.receptionist.Model.BaseApiResponse;
import com.example.my.receptionist.Model.PatientQueueResponse;
import com.example.my.receptionist.Model.ReceptionistData;
import com.example.my.receptionist.R;
import com.example.my.receptionist.interfaces.OnRowPopupOptionClicked;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by Dhillon on 05-10-2016.
 */

public class PatientQueueActivity extends BaseActivity implements AdapterView.OnItemClickListener, CustomPagerAdapter.PagerAdapterInterface, OnRowPopupOptionClicked {

    private List<String> tabs = new ArrayList<>();

    private CustomPagerAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ReceptionistData receptionistData;

    private TextView tvPatientCount;
    private ImageView ivHomeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_queue);
        receptionistData = ReceptionistData.getInstance();
        initTabs();

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new CustomPagerAdapter(getSupportFragmentManager(), tabs, this);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        ivHomeButton = (ImageView) findViewById(R.id.iv_home);

        tvPatientCount = (TextView) findViewById(R.id.tv_patient_count);
        tvPatientCount.setText("(0)");

//        setData();
        ivHomeButton.setOnClickListener(this);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getPatientQueue();
    }

    private void initTabs() {
        tabs.add(getString(R.string.all_patients));
        tabs.add(getString(R.string.doctors));
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_home:
                onBackPressed();
                break;
        }
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        switch (position) {
            case 0:
                AllPatientsFragments allPatientsFragments = AllPatientsFragments.getRunningInstance();
                allPatientsFragments.setOnRowPopupOptionClicked(this);
                return allPatientsFragments;
            case 1:
                DoctorFragment doctorFragment = DoctorFragment.getRunningInstance();
                doctorFragment.setOnRowPopupOptionClicked(this);
                return doctorFragment;

        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return (CharSequence) listItem;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null)
            return;
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_PATIENT_QUEUE:
                PatientQueueResponse patientQueueResponse = (PatientQueueResponse) response;
                if (false == patientQueueResponse.getError() && patientQueueResponse.getData() != null) {
//                    tvPatientCount.setText(" ("+patientQueueDataList.size()+")");
                    tvPatientCount.setText(" ("+patientQueueResponse.getData().size()+")");
                    AllPatientsFragments.getRunningInstance().setPatientQueueDataList(patientQueueResponse.getData());
                    AllPatientsFragments.getRunningInstance().refreshData();
                    DoctorFragment.getRunningInstance().setPatientQueueDataList(patientQueueResponse.getData());
                    DoctorFragment.getRunningInstance().refreshData();
                } else {
                    showToast(patientQueueResponse.getMessage());
                    tvPatientCount.setText("(0)");
                    AllPatientsFragments.getRunningInstance().setPatientQueueDataList(null);
                    AllPatientsFragments.getRunningInstance().refreshData();
                    DoctorFragment.getRunningInstance().setPatientQueueDataList(null);
                    DoctorFragment.getRunningInstance().refreshData();
                    return;
                }
                break;
            case AppConstants.TASK_CODES.DELETE_QUEUE_ENTRY:{
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if(baseApiResponse!=null){
                    if(baseApiResponse.getError()){
                        showToast(baseApiResponse.getMessage());
                    } else {
                        getPatientQueue();
                    }
                }
            }
            break;
        }
    }

    private void getPatientQueue() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_PATIENT_QUEUE);
        httpParamObject.addParameter("clinicId", receptionistData.getClinicId());
        httpParamObject.setClassType(PatientQueueResponse.class);
        executeTask(AppConstants.TASK_CODES.GET_PATIENT_QUEUE, httpParamObject);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    private void removeQueueEntry(String queueId){
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.DELETE_QUEUE_ENTRY+"?id="+queueId);
        httpParamObject.addParameter("id",queueId);
        httpParamObject.setPatchMethod();
        httpParamObject.setClassType(BaseApiResponse.class);
        httpParamObject.setJSONContentType();
        executeTask(AppConstants.TASK_CODES.DELETE_QUEUE_ENTRY, httpParamObject);
    }

    @Override
    public void removePatientQueueFromQueue(String queueId) {
        removeQueueEntry(queueId);
    }

    @Override
    public void changePhysician(String queueId, String physicianID) {
        Intent intent = new Intent(PatientQueueActivity.this, ChangePhysicianActivity.class);
        intent.putExtra(AppConstants.BUNDLE_KEYS.QUEUE_ID, queueId);
        intent.putExtra(AppConstants.BUNDLE_KEYS.PHYSICIAN_ID, physicianID);
        startActivityForResult(intent, AppConstants.REUEST_CODES.CHANGE_PHYSICIAN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode!=RESULT_OK){
            return;
        }
        switch (requestCode){
            case AppConstants.REUEST_CODES.CHANGE_PHYSICIAN:{
                getPatientQueue();
                break;
            }
        }
    }
}
