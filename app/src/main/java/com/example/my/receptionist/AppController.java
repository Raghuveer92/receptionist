package com.example.my.receptionist;

import android.app.Application;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import simplifii.framework.utility.Preferences;

/**
 * Created by Dhillon on 03-10-2016.
 */

public class AppController extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        Preferences.initSharedPreferences(this);
    }
}
