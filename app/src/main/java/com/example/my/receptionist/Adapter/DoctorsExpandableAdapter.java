package com.example.my.receptionist.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.my.receptionist.Model.DoctorHeader;
import com.example.my.receptionist.R;

import java.util.HashMap;
import java.util.List;

public class DoctorsExpandableAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<DoctorHeader> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<DoctorHeader>> _listDataChild;
 
    public DoctorsExpandableAdapter(Context context, List<DoctorHeader> listDataHeader,
                                    HashMap<String, List<DoctorHeader>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }
 
    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
 
        final String childText = (String) getChild(groupPosition, childPosition);
 
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_item_patient, null);
        }
 
        TextView txtListChild = (TextView) convertView.findViewById(R.id.tv_patient_name);
 
        txtListChild.setText(childText);
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }
 
    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_doctor, null);
        }

        TextView doctorName = (TextView) convertView.findViewById(R.id.tv_dr_name);
        TextView doctorType = (TextView) convertView.findViewById(R.id.tv_dr_spcl);
        TextView count = (TextView) convertView.findViewById(R.id.tv_item_count);

        doctorName.setTypeface(null, Typeface.BOLD);
        doctorName.setText(headerTitle);
 
        return convertView;
    }
 
    @Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}