package com.example.my.receptionist;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dhillon on 05-10-2016.
 */

public class ImageUploadTask extends AsyncTask<Bitmap, Void, String> {
    UploadListener uploadListener;
    private Bitmap bitmap;

    public static ImageUploadTask uploadImage(UploadListener uploadListener, Bitmap bitmap) {
        ImageUploadTask imageUploadTask = new ImageUploadTask();
        imageUploadTask.uploadListener = uploadListener;
        imageUploadTask.bitmap = bitmap;
        return imageUploadTask;
    }

    @Override
    protected String doInBackground(Bitmap... params) {
        String imageUrl = null;
        try {
            imageUrl = uploadViaCloudinary(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageUrl;
    }

    @Override
    protected void onPostExecute(String s) {
        if (s == null) {
            uploadListener.onUploadFailed();
        } else {
            uploadListener.onUpload(s);
        }
    }

    private String uploadViaCloudinary(Bitmap bMap) throws Exception {
        Map config = new HashMap();
        config.put("cloud_name", "dm7mirpdi");
        config.put("api_key", "851662549713963");
        config.put("api_secret", "cgCnxhnjyboiriSTBTjCVLHVKBk");
        Cloudinary cloudinary = new Cloudinary(config);
        FileInputStream fileInputStream = new FileInputStream(getFile(bMap));
        final Map map = cloudinary.uploader().upload(fileInputStream, ObjectUtils.emptyMap());
        if (map.containsKey("url")) {
            return map.get("url").toString();
        }
        throw new Exception();
    }

    private File getFile(Bitmap bMap) throws Exception {
        bMap = getResizedBitmap(bMap);
        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/Talluk";
        File dir = new File(file_path);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dir, System.currentTimeMillis() + ".png");
        FileOutputStream fOut = new FileOutputStream(file);

        bMap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
        fOut.flush();
        fOut.close();
        return file;
    }

    public Bitmap getResizedBitmap(Bitmap image) {
        int maxSize = 300;
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public interface UploadListener {
        void onUpload(String imageUrls);

        void onUploadFailed();
    }
}