package com.example.my.receptionist.Model;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Dhillon on 03-10-2016.
 */

public class PatientData implements Serializable{

    @SerializedName("patientId")
    @Expose
    private String patientId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("upId")
    @Expose
    private String upId;

    /**
     *
     * @return
     * The patientId
     */
    public String getPatientId() {
        return patientId;
    }

    /**
     *
     * @param patientId
     * The patientId
     */
    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The age
     */
    public String getDateOfBirth() {
        return age;
    }

    /**
     *
     * @param age
     * The age
     */
    public void setDateOfBirth(String age) {
        this.age = age;
    }

    /**
     *
     * @return
     * The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     * The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     * The emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     *
     * @param emailId
     * The emailId
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     *
     * @return
     * The phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *
     * @param phoneNumber
     * The phoneNumber
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *
     * @return
     * The upId
     */
    public String getUpId() {
        return upId;
    }

    /**
     *
     * @param upId
     * The upId
     */
    public void setUpId(String upId) {
        this.upId = upId;
    }

    public static PatientData getInstance(){
        Gson gson = new Gson();
        String data = Preferences.getData(AppConstants.PREF_KEYS.PATIENT_DATA,"");
        if(!TextUtils.isEmpty(data)){
            PatientData patientData = gson.fromJson(data, PatientData.class);
            return patientData;
        }
        return null;
    }

}