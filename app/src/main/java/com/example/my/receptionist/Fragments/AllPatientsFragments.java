package com.example.my.receptionist.Fragments;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.my.receptionist.Activity.ChangePhysicianActivity;
import com.example.my.receptionist.Activity.PatientQueueActivity;
import com.example.my.receptionist.Model.PatientQueueData;
import com.example.my.receptionist.Model.PatientQueueResponse;
import com.example.my.receptionist.Model.ReceptionistData;
import com.example.my.receptionist.R;
import com.example.my.receptionist.interfaces.OnRowPopupOptionClicked;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by Neeraj Yadav on 12/22/2016.
 */

public class AllPatientsFragments extends BaseFragment implements CustomListAdapterInterface {
    private ListView patientList;
    private List<PatientQueueData> patientQueueDataList = new ArrayList<>();
    private ReceptionistData receptionistData;
    private CustomListAdapter patientAdapter;
    private OnRowPopupOptionClicked onRowPopupOptionClicked;

    private static AllPatientsFragments runningInstance;

    public static AllPatientsFragments getRunningInstance() {
        if(runningInstance==null)
            runningInstance = new AllPatientsFragments();
        return runningInstance;
    }

    public void setOnRowPopupOptionClicked(OnRowPopupOptionClicked onRowPopupOptionClicked) {
        this.onRowPopupOptionClicked = onRowPopupOptionClicked;
    }

    public void setPatientQueueDataList(List<PatientQueueData> patientQueueDataList) {
        this.patientQueueDataList.clear();
        if(CollectionUtils.isNotEmpty(patientQueueDataList))
            this.patientQueueDataList.addAll(patientQueueDataList);
    }

    @Override
    public void initViews() {

        receptionistData = ReceptionistData.getInstance();

        patientList = (ListView) findView(R.id.lv_patient_queue);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

        patientAdapter = new CustomListAdapter(getActivity(), R.layout.row_item_patient, patientQueueDataList, this);
        patientList.setAdapter(patientAdapter);
//        patientList.setOnItemClickListener(getActivity().this);
    }

    @Override
    public void refreshData() {
        patientAdapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_all_patients;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final PatientQueueData patientQueueData = this.patientQueueDataList.get(position);
        if (!TextUtils.isEmpty(patientQueueData.getPatientDetails().getImageUrl()))
            Picasso.with(getActivity()).load(patientQueueData.getPatientDetails().getImageUrl()).placeholder(R.drawable.ic_male).into(holder.ivProfilePic);
        else
            holder.ivProfilePic.setImageResource(R.drawable.ic_male);

        holder.tvName.setText(patientQueueData.getPatientDetails().getName());
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(patientQueueData.getQueueDetails().getAppointmentTime());
        holder.tvTime.setText(pad(cal.get((Calendar.HOUR_OF_DAY))) + ":" + pad(cal.get(Calendar.MINUTE)));
        if(!TextUtils.isEmpty(patientQueueData.getPhysicianDetails().getProfile().getFullname())){
            holder.tvPhysicianName.setText(patientQueueData.getPhysicianDetails().getProfile().getFullname());
        } else {
            holder.tvPhysicianName.setText("");
        }
        holder.iv_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getActivity(), holder.iv_option);
                popup.getMenuInflater().inflate(R.menu.patient_queue_popup, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.change_physician:
                                if(onRowPopupOptionClicked!=null){
                                    onRowPopupOptionClicked.changePhysician(patientQueueData.getQueueDetails().getQueueId(),patientQueueData.getPhysicianDetails().getPhysicianId());
                                }
                                break;
                            case R.id.remove_patient:
//                                Toast.makeText(getActivity(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                                if(onRowPopupOptionClicked!=null){
                                    onRowPopupOptionClicked.removePatientQueueFromQueue(patientQueueData.getQueueDetails().getQueueId());
                                }
                                break;
                        }
                        return true;
                    }
                });

                popup.show();
            }
        });

        return convertView;
    }

    private String pad(int value) {

        if (value < 10) {
            return "0" + value;
        }
        return "" + value;
    }


    private class Holder {
        ImageView ivProfilePic, iv_option;
        TextView tvName;
        TextView tvTime;
        TextView tvPhysicianName;

        public Holder(View view) {
            ivProfilePic = (ImageView) view.findViewById(R.id.iv_patient_profile_pic);
            tvName = (TextView) view.findViewById(R.id.tv_patient_name);
            tvTime = (TextView) view.findViewById(R.id.tv_appointment_time);
            iv_option = (ImageView) view.findViewById(R.id.iv_option);
            tvPhysicianName = (TextView) view.findViewById(R.id.tv_doctor_name);
        }
    }




}
