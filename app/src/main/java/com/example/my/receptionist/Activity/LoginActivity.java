package com.example.my.receptionist.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.my.receptionist.Model.CreateReceptionistApi;
import com.example.my.receptionist.R;
import com.example.my.receptionist.utils.RestApiUtil;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class LoginActivity extends BaseActivity{

    private Button buttonLogin;
    private EditText etNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etNumber = (EditText) findViewById(R.id.et_number);

        buttonLogin = (Button) findViewById(R.id.btn_login);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = etNumber.getText().toString().trim();
                if(number.length()!=10){
                    showToast(getString(R.string.error_invalid_mobile));
                    return;
                }
                HttpParamObject httpParamObject  = RestApiUtil.findReceptionistByNumber(number);
                executeTask(AppConstants.TASK_CODES.FIND_RECEPTIONIST_BY_NUMBER, httpParamObject);
            }
        });
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response==null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.FIND_RECEPTIONIST_BY_NUMBER:{
                CreateReceptionistApi createReceptionistApi = (CreateReceptionistApi) response;
                if(createReceptionistApi!=null){
                    if(!createReceptionistApi.getError()){
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.RECEPTIONIST_DATA,createReceptionistApi.getData());
                        startNextActivity(bundle,OtpActivity.class);
                    } else {
                        showToast(createReceptionistApi.getMessage());
                    }
                }
                break;
            }
        }
    }
}
