package com.example.my.receptionist.Model.clinic;

import com.example.my.receptionist.Model.BaseApiResponse;

import java.util.List;

/**
 * Created by raghu on 16/1/17.
 */

public class ClinicResponse extends BaseApiResponse {
    List<ClinicData> data;

    public List<ClinicData> getData() {
        return data;
    }
}
