package com.example.my.receptionist.Activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.my.receptionist.Model.BaseApiResponse;
import com.example.my.receptionist.Model.GetPhysician;
import com.example.my.receptionist.Model.PhysicianBasicInfo;
import com.example.my.receptionist.Model.ReceptionistData;
import com.example.my.receptionist.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class ChangePhysicianActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private List<PhysicianBasicInfo> physicianBasicInfoList = new ArrayList<>();
    private CustomListAdapter physicianAdapter;
    private ListView lv;
    private ReceptionistData receptionistData;
    private String selectPhysicianId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_physician);
        selectPhysicianId = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.PHYSICIAN_ID);

        initToolBar("Change Physician");
        getHomeIcon();

        receptionistData = ReceptionistData.getInstance();
        getPhysicianList();


        lv = (ListView) findViewById(R.id.lv_change_physician);
        physicianAdapter = new CustomListAdapter(this, R.layout.row_lv_home_activity, physicianBasicInfoList, this);
        lv.setAdapter(physicianAdapter);
        lv.setOnItemClickListener(this);
        setOnClickListener(R.id.btn_done);
    }

    private void getPhysicianList() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_PHYSICIAN);
        httpParamObject.addParameter("id", receptionistData.getClinicId());
        httpParamObject.setClassType(GetPhysician.class);
        executeTask(AppConstants.TASK_CODES.GET_PHYSICIAN, httpParamObject);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_done:
                if (getSelectedPhysicianId() != null) {
                    validateAndChangePhysician();
                } else {
                    showToast("Please select a physician");
                }
                break;
        }
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        PhysicianBasicInfo physicianBasicInfo = physicianBasicInfoList.get(position);
        if (physicianBasicInfo.isSelected()) {
            if (!TextUtils.isEmpty(physicianBasicInfo.getImageUrl()))
                Picasso.with(this).load(physicianBasicInfo.getImageUrl()).placeholder(R.drawable.ic_male_orange).into(holder.iv_profilepic);
            else {
                holder.iv_profilepic.setImageResource(R.drawable.ic_male_orange);
            }
            convertView.setBackgroundColor(getResourceColor(R.color.selected_orange));
        } else {
            if (!TextUtils.isEmpty(physicianBasicInfo.getImageUrl()))
                Picasso.with(this).load(physicianBasicInfo.getImageUrl()).placeholder(R.drawable.ic_male).into(holder.iv_profilepic);
            else {
                holder.iv_profilepic.setImageResource(R.drawable.ic_male);
            }
            convertView.setBackgroundColor(getResourceColor(R.color.off_white));
        }
        holder.tv_name.setText(physicianBasicInfo.getName());

        if (CollectionUtils.isNotEmpty(physicianBasicInfo.getSpecializations())) {
            StringBuilder stringBuilder = new StringBuilder("");
            for (String str : physicianBasicInfo.getSpecializations()) {
                stringBuilder.append(str).append(", ");
            }

            if (stringBuilder.length() > 0) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }
            holder.tv_specialization.setText(stringBuilder.toString());
        } else {
            holder.tv_specialization.setText("");
        }
        return convertView;
    }

    private void unselectAll() {
        for (PhysicianBasicInfo list : physicianBasicInfoList) {
            list.setSelected(false);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        unselectAll();
        physicianBasicInfoList.get(position).setSelected(true);
        physicianAdapter.notifyDataSetChanged();
    }

    class Holder {
        TextView tv_name;
        TextView tv_specialization;
        ImageView iv_profilepic;

        public Holder(View view) {
            tv_name = (TextView) view.findViewById(R.id.tv_physician_name);
            tv_specialization = (TextView) view.findViewById(R.id.tv_physician_special);
            iv_profilepic = (ImageView) view.findViewById(R.id.iv_physician_profilepic);
        }
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.cross;
    }

    private void validateAndChangePhysician(){
        if(getIntent()!=null){
            String queueId = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.QUEUE_ID);
            if(!TextUtils.isEmpty(queueId)){
                HttpParamObject httpParamObject = new HttpParamObject();
                String url = AppConstants.PAGE_URL.CHANGE_PHYSICIAN + "?queueId=" + queueId + "&newPhysicianId=" + getSelectedPhysicianId()
                        + "&receptionistId=" + receptionistData.getReceptionistId();
                httpParamObject.setUrl(url);
                httpParamObject.setPatchMethod();
                httpParamObject.setClassType(BaseApiResponse.class);
                executeTask(AppConstants.TASK_CODES.CHANGE_PHYSICIAN, httpParamObject);
            }
        }

    }

    private String getSelectedPhysicianId() {
        for (PhysicianBasicInfo physicianBasicInfo: physicianBasicInfoList){
            if(physicianBasicInfo.isSelected()){
                return physicianBasicInfo.getPhysicianId();
            }
        }
        return null;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response)
            return;
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_PHYSICIAN:
                GetPhysician getPhysician = (GetPhysician) response;
                if (false == getPhysician.getError() && getPhysician.getClinicPhysicianData() != null) {
//                    if (getPhysician.getClinicPhysicianData().getClinicId() == receptionistData.getClinicId()) {
                    physicianBasicInfoList.clear();
                    physicianBasicInfoList.addAll(getPhysician.getClinicPhysicianData().getPhysicianList());
                    physicianAdapter.notifyDataSetChanged();
                    setDefaultPhysician(physicianBasicInfoList);
                    physicianAdapter.notifyDataSetChanged();
//                    } else {
//                        showToast("Invalid Clinic Id");
//                    }
                } else {
                    showToast(getPhysician.getMessage());
                    return;
                }
                break;
            case AppConstants.TASK_CODES.CHANGE_PHYSICIAN:
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if(baseApiResponse!=null){
                    if(!baseApiResponse.getError()){
                        if(!selectPhysicianId.equals(getSelectedPhysicianId())){
                            showToast("Physician successfully changed !");
                        }
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        showToast(baseApiResponse.getMessage());
                    }
                }
        }
    }

    private void setDefaultPhysician(List<PhysicianBasicInfo> physicianBasicInfos) {
        if(!TextUtils.isEmpty(selectPhysicianId)){
            for(PhysicianBasicInfo physicianBasicInfo:physicianBasicInfos){
                String id = physicianBasicInfo.getPhysicianId();
                if(selectPhysicianId.equalsIgnoreCase(id)){
                    physicianBasicInfo.setSelected(true);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Util.createAlertDialog(this, "Abort adding patient to queue", "Exit", false, "Abort", "Exit", new Util.DialogListener() {
//            @Override
//            public void onOKPressed(DialogInterface dialog, int which) {
//                showToast("Patient not added to queue");
//                Intent i = new Intent(ChangePhysicianActivity.this, HomeActivity.class);
//                // set the new task and clear flags
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(i);
//                dialog.dismiss();
//            }
//
//            @Override
//            public void onCancelPressed(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        }).show();
    }

}
