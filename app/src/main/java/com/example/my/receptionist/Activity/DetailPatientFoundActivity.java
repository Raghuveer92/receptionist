package com.example.my.receptionist.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

import android.graphics.Color;

import com.example.my.receptionist.Model.PatientData;
import com.example.my.receptionist.R;
import com.squareup.picasso.Picasso;

public class DetailPatientFoundActivity extends BaseActivity {

    private TextView tvName, tvFullname, tvAge, tvGender, tvEmail, tvPhone, tvUpid;
    private Button btnAddToQueue;
    ImageView ivPatientPic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_found);
        initToolBar("Patient Found");
        tvName = (TextView) findViewById(R.id.tv_patient_name);
        tvFullname = (TextView) findViewById(R.id.tv_patient_fullname);
        tvAge = (TextView) findViewById(R.id.tv_patient_dob);
        tvGender = (TextView) findViewById(R.id.tv_patient_gender);
        tvEmail = (TextView) findViewById(R.id.tv_patient_email);
        tvPhone = (TextView) findViewById(R.id.tv_patient_mobile);
        tvUpid = (TextView) findViewById(R.id.tv_patient_upid);
        btnAddToQueue = (Button) findViewById(R.id.btn_add_to_queue);
        ivPatientPic = (ImageView) findViewById(R.id.iv_patient);
        btnAddToQueue.setOnClickListener(this);
        setDefaultData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                startNextActivity(HomeActivity.class);
                finish();
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_add_to_queue:
                Intent intent = new Intent(DetailPatientFoundActivity.this, SelectPhysicianActivity.class);
                intent.putExtras(getIntent());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void setDefaultData() {
        if(getIntent()!=null) {
            PatientData patientData = (PatientData)getIntent().getSerializableExtra(AppConstants.BUNDLE_KEYS.PATIENT_DATA);
            if (patientData != null) {
                if (!TextUtils.isEmpty(patientData.getImageUrl()))
                    Picasso.with(DetailPatientFoundActivity.this).load(patientData.getImageUrl()).placeholder(R.drawable.ic_male_orange).into(ivPatientPic);
                tvName.setText(resolveText(patientData.getName()));
                tvFullname.setText(resolveText(patientData.getName()));
                tvGender.setText(resolveText(patientData.getGender()));
                setSelect(tvGender);
                tvPhone.setText(resolveText(patientData.getPhoneNumber()));
                tvUpid.setText(resolveText(patientData.getUpId()));
                tvEmail.setText(resolveText(patientData.getEmailId()));
                tvAge.setText(resolveText(patientData.getDateOfBirth()));
            }
        }
    }

    private String resolveText(String text) {
        if (TextUtils.isEmpty(text)) {
            return getString(R.string.not_available);
        }
        return text;
    }

    private void setSelect(TextView gender) {
        gender.setTextColor(getResourceColor(R.color.orange));
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.back_arrow;
    }
}
