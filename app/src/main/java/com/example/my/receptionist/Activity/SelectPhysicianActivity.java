package com.example.my.receptionist.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;
import android.widget.ListView;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.my.receptionist.Model.AddToQueue;
import com.example.my.receptionist.Model.GetPhysician;
import com.example.my.receptionist.Model.PatientData;
import com.example.my.receptionist.Model.PhysicianBasicInfo;
import com.example.my.receptionist.Model.PhysicianData;
import com.example.my.receptionist.Model.ReceptionistData;
import com.example.my.receptionist.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.ListAdapters.CustomListAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

public class SelectPhysicianActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private List<PhysicianBasicInfo> physicianData = new ArrayList<>();
    private CustomListAdapter physicianAdapter;
    private ListView lv;
    private ReceptionistData receptionistData;
    private PatientData patientData;
    private PhysicianBasicInfo selectedPhysician;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_physician);
        initToolBar("Select Physician");
        getHomeIcon();

        receptionistData = ReceptionistData.getInstance();
        if(getIntent()!=null)
            patientData = (PatientData)getIntent().getSerializableExtra(AppConstants.BUNDLE_KEYS.PATIENT_DATA);
        getPhysicianList();

        lv = (ListView) findViewById(R.id.lv_select_physician);
        physicianAdapter = new CustomListAdapter(this, R.layout.row_lv_home_activity, physicianData, this);
        lv.setAdapter(physicianAdapter);
        lv.setOnItemClickListener(this);
        setOnClickListener(R.id.btn_done);
    }

    private void getPhysicianList() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_PHYSICIAN);
        httpParamObject.addParameter("id", receptionistData.getClinicId());
        httpParamObject.setClassType(GetPhysician.class);
        executeTask(AppConstants.TASK_CODES.GET_PHYSICIAN, httpParamObject);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_done:
                if (selectedPhysician != null) {
                    AddToQueue();
                    showToast("Adding to queue...", Toast.LENGTH_SHORT);
                } else {
                    showToast("Please select a physician");
                }
                break;
        }
    }

    private void AddToQueue() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.ADD_TO_QUEUE);
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(getQueueData().toString());
        httpParamObject.setPostMethod();
        httpParamObject.setClassType(AddToQueue.class);
        executeTask(AppConstants.TASK_CODES.ADD_TO_QUEUE, httpParamObject);
    }

    private JSONObject getQueueData() {
        JSONObject jsonObject = new JSONObject();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try {
            jsonObject.put("clinicId", receptionistData.getClinicId());
            jsonObject.put("patientId", patientData.getPatientId());
            jsonObject.put("physicianId", selectedPhysician.getPhysicianId());
            jsonObject.put("appointmentTime", sdf.format(new Date()).toString());
            jsonObject.put("createdBy",receptionistData.getReceptionistId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        PhysicianBasicInfo physicianData = this.physicianData.get(position);
        if (physicianData.isSelected()) {
            if (!TextUtils.isEmpty(physicianData.getImageUrl()))
                Picasso.with(this).load(physicianData.getImageUrl()).placeholder(R.drawable.ic_male_orange).into(holder.iv_profilepic);
            else {
                holder.iv_profilepic.setImageResource(R.drawable.ic_male_orange);
            }
        } else {
            if (!TextUtils.isEmpty(physicianData.getImageUrl()))
                Picasso.with(this).load(physicianData.getImageUrl()).placeholder(R.drawable.ic_male).into(holder.iv_profilepic);
            else {
                holder.iv_profilepic.setImageResource(R.drawable.ic_male);
            }
        }
        holder.tv_name.setText(physicianData.getName());

        if (CollectionUtils.isNotEmpty(physicianData.getSpecializations())) {
            StringBuilder stringBuilder = new StringBuilder("");
            for (String str : physicianData.getSpecializations()) {
                stringBuilder.append(str).append(", ");
            }

            if (stringBuilder.length() > 0) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }
            holder.tv_specialization.setText(stringBuilder.toString());
        } else {
            holder.tv_specialization.setText("");
        }
        return convertView;
    }

    private void unselectAll() {
        for (PhysicianBasicInfo list : physicianData) {
            list.setSelected(false);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        unselectAll();
        physicianData.get(position).setSelected(true);
        selectedPhysician = physicianData.get(position);
        physicianAdapter.notifyDataSetChanged();
    }

    class Holder {
        TextView tv_name;
        TextView tv_specialization;
        ImageView iv_profilepic;

        public Holder(View view) {
            tv_name = (TextView) view.findViewById(R.id.tv_physician_name);
            tv_specialization = (TextView) view.findViewById(R.id.tv_physician_special);
            iv_profilepic = (ImageView) view.findViewById(R.id.iv_physician_profilepic);
        }
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.cross;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response)
            return;
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_PHYSICIAN:
                GetPhysician getPhysician = (GetPhysician) response;
                if (false == getPhysician.getError() && getPhysician.getClinicPhysicianData() != null) {
//                    if (getPhysician.getClinicPhysicianData().getClinicId() == receptionistData.getClinicId()) {
                    physicianData.clear();
                    physicianData.addAll(getPhysician.getClinicPhysicianData().getPhysicianList());
                    physicianAdapter.notifyDataSetChanged();
//                    } else {
//                        showToast("Invalid Clinic Id");
//                    }
                } else {
                    showToast(getPhysician.getMessage());
                    return;
                }
                break;
            case AppConstants.TASK_CODES.ADD_TO_QUEUE:
                AddToQueue addToQueue = (AddToQueue) response;
                if (false == addToQueue.getError()) {
                    showToast("Added successfully!");
                    startNextActivity(HomeActivity.class);
                    finish();
                } else {
                    showToast(addToQueue.getMessage());
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Util.createAlertDialog(this, "Abort adding patient to queue", "Exit", false, "Abort", "Exit", new Util.DialogListener() {
            @Override
            public void onOKPressed(DialogInterface dialog, int which) {
                showToast("Patient not added to queue");
                Intent i = new Intent(SelectPhysicianActivity.this, HomeActivity.class);
                // set the new task and clear flags
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                dialog.dismiss();
            }

            @Override
            public void onCancelPressed(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }
}

