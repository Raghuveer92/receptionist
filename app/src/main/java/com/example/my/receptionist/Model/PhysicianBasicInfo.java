package com.example.my.receptionist.Model;

import java.util.List;

/**
 * Created by robin on 12/23/16.
 */

public class PhysicianBasicInfo {

    private String physicianId;
    private String name;
    private String imageUrl;
    private List<String> specializations;

    private boolean isSelected;

    public String getPhysicianId() {
        return physicianId;
    }

    public void setPhysicianId(String physicianId) {
        this.physicianId = physicianId;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<String> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(List<String> specializations) {
        this.specializations = specializations;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String toString() {
        return "PhysicianBasicInfoDto [physicianId=" + physicianId + ", name=" + name + ", imageUrl=" + imageUrl
                + ", specializations=" + specializations + "]";
    }

}
