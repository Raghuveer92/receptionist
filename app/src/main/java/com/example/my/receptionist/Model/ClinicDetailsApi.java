package com.example.my.receptionist.Model;

import com.example.my.receptionist.Model.clinic.ClinicData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

/**
 * Created by Dhillon on 03-10-2016.
 */

public class ClinicDetailsApi {

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("data")
    @Expose
    private List<ClinicData> data = new ArrayList();
    @SerializedName("message")
    @Expose
    private String message;

    /**
     * @return The error
     */
    public boolean getError() {
        return error;
    }

    /**
     * @param error The error
     */
    public void setError(boolean error) {
        this.error = error;
    }

    /**
     * @return The code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return The data
     */
    public List<ClinicData> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<ClinicData> data) {
        this.data = data;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }


}