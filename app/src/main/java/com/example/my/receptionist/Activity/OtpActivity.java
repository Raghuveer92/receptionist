package com.example.my.receptionist.Activity;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.my.receptionist.Model.GetAuthKeyApi;
import com.example.my.receptionist.Model.OTPData;
import com.example.my.receptionist.Model.ReceptionistData;
import com.example.my.receptionist.Model.SendOtpResponse;
import com.example.my.receptionist.R;
import com.example.my.receptionist.utils.RestApiUtil;
import com.squareup.picasso.Picasso;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class OtpActivity extends BaseActivity {

    private ReceptionistData receptionistData;
    private int otp;
    private EditText etPin;
    private Button btnLogin;
    private TextView tvReceptionistName;
    private ImageView ivReceptionistImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        etPin = (EditText) findViewById(R.id.et_pin);
        btnLogin = (Button) findViewById(R.id.btn_login);

        tvReceptionistName = (TextView) findViewById(R.id.tv_receptionist_name);
        ivReceptionistImage = (ImageView) findViewById(R.id.iv_receptionist_image);

        if(getIntent()!=null&&getIntent().getExtras()!=null) {
            receptionistData = (ReceptionistData) getIntent().getExtras().getSerializable(AppConstants.BUNDLE_KEYS.RECEPTIONIST_DATA);
            if (receptionistData != null) {
                if (!TextUtils.isEmpty(receptionistData.getName())) {
                    tvReceptionistName.setText(receptionistData.getName());
                } else {
                    tvReceptionistName.setText("Receptionist");
                }
                if (!TextUtils.isEmpty(receptionistData.getImageUrl())) {
                    Picasso.with(this).load(receptionistData.getImageUrl()).placeholder(R.drawable.ic_male).into(ivReceptionistImage);
                } else {
                    ivReceptionistImage.setImageResource(R.drawable.ic_male);
                }
            }
        }
        btnLogin.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_login:{
                String pin = etPin.getText().toString().trim();
                if(TextUtils.isEmpty(pin)||!pin.equals(String.valueOf(otp))){
                    showToast(getString(R.string.invalid_pin));
                    return;
                }
                String receptionistJson = JsonUtil.getJsonString(receptionistData);
                Preferences.saveData(AppConstants.PREF_KEYS.RECEPTIONIST_DATA, receptionistJson);
                Preferences.saveData(AppConstants.PREF_KEYS.IS_LOGIN, true);
                Intent intent = new Intent(this,HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                break;
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        sendOtp();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response==null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.SEND_OTP:
                SendOtpResponse sendOtpResponse= (SendOtpResponse) response;
                if(sendOtpResponse!=null){
                    if(sendOtpResponse.getError()){
                        showToast(sendOtpResponse.getMessage());
                        return;
                    }
                    OTPData otpData = sendOtpResponse.getData();
                    if(otpData!=null){
                        String oneTimePassword = otpData.getOneTimePassword();
                        try {
                            otp= Integer.parseInt(oneTimePassword);
                        }catch (NumberFormatException e){}
                        if(otpData.isDebugMode()){
                            showToast("Debug mode is on, your pin is "+ oneTimePassword);
                        }
                    }}
            break;
        }
    }

    private void sendOtp() {
        if (receptionistData != null) {
            HttpParamObject httpParamObject = RestApiUtil.generateAndSendOtp(receptionistData.getPhoneNumber());
            executeTask(AppConstants.TASK_CODES.SEND_OTP, httpParamObject);
        }
    }
}
