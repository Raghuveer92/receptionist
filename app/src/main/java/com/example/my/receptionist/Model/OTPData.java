package com.example.my.receptionist.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by raghu on 15/1/17.
 */

public class OTPData extends BaseApiResponse {
    @SerializedName("oneTimePassword")
    private String oneTimePassword;
    @SerializedName("type")
    private String type;
    @SerializedName("debugMode")
    private boolean debugMode;

    public String getOneTimePassword() {
        return oneTimePassword;
    }

    public void setOneTimePassword(String oneTimePassword) {
        this.oneTimePassword = oneTimePassword;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isDebugMode() {
        return debugMode;
    }

    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }
}
