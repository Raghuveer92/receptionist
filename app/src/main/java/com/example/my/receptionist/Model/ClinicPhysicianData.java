package com.example.my.receptionist.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dhillon on 05-10-2016.
 */

public class ClinicPhysicianData {

    @SerializedName("clinicId")
    @Expose
    private String clinicId;
    @SerializedName("physiciansList")
    @Expose
    private List<PhysicianBasicInfo> physiciansList = new ArrayList<>();

    /**
     *
     * @return
     * The clinicId
     */
    public String getClinicId() {
        return clinicId;
    }

    /**
     *
     * @param clinicId
     * The clinicId
     */
    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    /**
     *
     * @return
     * The physiciansList
     */
    public List<PhysicianBasicInfo> getPhysicianList() {
        return physiciansList;
    }

    /**
     *
     * @param physiciansList
     * The physiciansList
     */
    public void setPhysicianList(List<PhysicianBasicInfo> physiciansList) {
        this.physiciansList = physiciansList;
    }

}