package com.example.my.receptionist.Model;

import java.io.Serializable;

/**
 * Created by Neeraj Yadav on 12/22/2016.
 */

public class PatientModel implements Serializable {
    String pName, time;
    String userIcon;
    String queueId;

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public String getQueueId() {
        return queueId;
    }

    public void setQueueId(String queueId) {
        this.queueId = queueId;
    }
}
