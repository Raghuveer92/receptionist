package com.example.my.receptionist.Model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Certificates {

    @SerializedName("specializationCertiUrls")
    @Expose
    private List<String> specializationCertiUrls = null;
    @SerializedName("letterHeadUrl")
    @Expose
    private String letterHeadUrl;
    @SerializedName("digitalSignatureUrl")
    @Expose
    private String digitalSignatureUrl;

    public List<String> getSpecializationCertiUrls() {
        return specializationCertiUrls;
    }

    public void setSpecializationCertiUrls(List<String> specializationCertiUrls) {
        this.specializationCertiUrls = specializationCertiUrls;
    }

    public String getLetterHeadUrl() {
        return letterHeadUrl;
    }

    public void setLetterHeadUrl(String letterHeadUrl) {
        this.letterHeadUrl = letterHeadUrl;
    }

    public String getDigitalSignatureUrl() {
        return digitalSignatureUrl;
    }

    public void setDigitalSignatureUrl(String digitalSignatureUrl) {
        this.digitalSignatureUrl = digitalSignatureUrl;
    }

}