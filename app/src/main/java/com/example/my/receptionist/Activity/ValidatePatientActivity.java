package com.example.my.receptionist.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.my.receptionist.Model.FindByNumber;
import com.example.my.receptionist.Model.FindByNumberAndMatchPrefix;
import com.example.my.receptionist.R;
import com.example.my.receptionist.utils.RestApiUtil;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class ValidatePatientActivity extends BaseActivity implements TextWatcher {

    EditText editTextLetters, editTextPhone;
    Button btn_reg, btnRetry;
    LinearLayout layoutRetryRegister, layoutDotBar;
    private TextView tvPatientNotfound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_patient);
        initToolBar("Add Patient To Queue");
        editTextLetters = (EditText) findViewById(R.id.et_last_four_letters);
        layoutRetryRegister = (LinearLayout) findViewById(R.id.ll_reg_retry);
        layoutDotBar = (LinearLayout) findViewById(R.id.ll_dotbar1);
        editTextPhone = (EditText) findViewById(R.id.et_pat_mob_no);
        btn_reg = (Button) findViewById(R.id.btn_register);
        btnRetry = (Button) findViewById(R.id.btn_retry);
        tvPatientNotfound = (TextView) findViewById(R.id.tv_patient_not_found);
        editTextPhone.addTextChangedListener(this);
        setOnClickListener(R.id.btn_register,R.id.btn_retry);
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.home_button;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_register:
                Intent intent = new Intent(ValidatePatientActivity.this,PatientRegistrationActivity.class);
                intent.putExtra(AppConstants.REDIRECT_TO, SelectPhysicianActivity.class.getSimpleName());
                startActivity(intent);
                finish();
                break;
            case R.id.btn_retry:{
                validatePhoneNumber(editTextPhone.getText().toString());
                break;
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (editTextPhone.length() == 1) {
            layoutDotBar.setVisibility(View.VISIBLE);
            return;
        }
        if (editTextPhone.length() == 5) {
            layoutDotBar.setVisibility(View.GONE);
            layoutRetryRegister.setVisibility(View.VISIBLE);
            return;
        }
        if (editTextPhone.length() == 10) {
            layoutRetryRegister.setVisibility(View.GONE);
            validatePhoneNumber(editTextPhone.getText().toString());
            return;
        }
    }

    private void validatePhoneNumber(String phoneNumber) {
        //Check for all the digits
        HttpParamObject httpParamObject = RestApiUtil.findPatientByNumberAndMatchPrefixRequest(phoneNumber,editTextLetters.getText().toString().trim());
        executeTask(AppConstants.TASK_CODES.FIND_PATIENT_BY_NUMBER, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.FIND_PATIENT_BY_NUMBER:
                FindByNumberAndMatchPrefix findByNumber = (FindByNumberAndMatchPrefix) response;
                if (findByNumber != null) {
                    if (false == findByNumber.getError()) {
                        Intent intent = new Intent(ValidatePatientActivity.this, EnterPinActivity.class);
                        intent.putExtra(AppConstants.BUNDLE_KEYS.PATIENT_DATA, findByNumber.getData());
                        startActivity(intent);
                    }  else {
                        showToast(findByNumber.getMessage());
                        layoutRetryRegister.setVisibility(View.VISIBLE);
                    }
                }
        }
    }
}

