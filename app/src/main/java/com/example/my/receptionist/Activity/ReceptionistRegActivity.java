package com.example.my.receptionist.Activity;

import android.app.DatePickerDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.my.receptionist.Fragments.MediaFragment;
import com.example.my.receptionist.ImageUploadTask;
import com.example.my.receptionist.Model.CreateReceptionistApi;
import com.example.my.receptionist.Model.clinic.Address;
import com.example.my.receptionist.Model.clinic.ClinicData;
import com.example.my.receptionist.Model.clinic.ClinicResponse;
import com.example.my.receptionist.R;
import com.example.my.receptionist.utils.FileUploadTask;
import com.example.my.receptionist.utils.RestApiUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class ReceptionistRegActivity extends BaseActivity implements  CustomListAdapterInterface,AdapterView.OnItemSelectedListener {
    private int PICK_IMAGE_REQUEST = 1;

    private int mYear, mMonth, mDay;
    private TextView tvMale, tvFemale;
    String gender = null;
    private ClinicData selectedClinic = new ClinicData();
    private int selectedClinicIndex = -1;
    private MediaFragment imagePicker;
    private Bitmap profileBitmap;
    private ImageView dobIcon;
    private ImageView profilePic;
    private String profileImageUrl;
    private Spinner spinnerCity;
    private Spinner spinnerClinic;
    private List<ClinicData> clinicDataList;
    private ArrayAdapter<String> clinicAdapter;
    private List<String> listCity = new ArrayList<>();
    private List<String> listClinic = new ArrayList<>();
    private HashMap<String, ArrayList<ClinicData>> citiesMap = new HashMap();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receptionist_reg);
        initToolBar(getString(R.string.registration));
        getHomeIcon();

        profilePic = (ImageView) findViewById(R.id.iv_patient_profile_pic);
        spinnerCity = (Spinner) findViewById(R.id.spinner_city);
        spinnerClinic = (Spinner) findViewById(R.id.spinner_clinic);
        spinnerCity.setOnItemSelectedListener(this);

        tvMale = (TextView) findViewById(R.id.tv_male);
        tvFemale = (TextView) findViewById(R.id.tv_female);

        dobIcon = (ImageView) findViewById(R.id.dob_icon);

        imagePicker = new MediaFragment();
        getSupportFragmentManager().beginTransaction().add(imagePicker, "Profile Pic").commit();

        spinnerCity = (Spinner) findViewById(R.id.spinner_city);
        spinnerClinic = (Spinner) findViewById(R.id.spinner_clinic);
        setClinicListAdapter();
        spinnerCity.setOnItemSelectedListener(this);

        setOnClickListener(R.id.tv_male, R.id.tv_female, R.id.btn_reg, R.id.tv_name_dob, R.id.iv_patient_profile_pic);
    }

    private void setClinicListAdapter() {
        clinicAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, listClinic);
        clinicAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerClinic.setAdapter(clinicAdapter);
    }

    private void setCityList() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, listCity);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCity.setAdapter(dataAdapter);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        fetchClinics();
    }

    private void fetchClinics() {
        HttpParamObject httpParamObject = RestApiUtil.getAllClinicsRequest();
        executeTask(AppConstants.TASK_CODES.GET_ALL_CLINICS, httpParamObject);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_male:
                unselectAllGenders();
                setSelect(tvMale);
                break;
            case R.id.tv_female:
                unselectAllGenders();
                setSelect(tvFemale);
                break;
            case R.id.btn_reg:
                submitData();
                break;
            case R.id.tv_name_dob:
                selectDate();
                break;
            case R.id.iv_patient_profile_pic:
                imagePicker.getImage(new MediaFragment.ImageListener() {
                    @Override
                    public void onGetBitMap(Bitmap bitmap) {
                        profileBitmap = bitmap;
                        profilePic.setImageBitmap(profileBitmap);
                    }
                });
        }
    }

    private void selectDate() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(ReceptionistRegActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year,R.id.tv_name_dob);
                        dobIcon.setImageResource(R.drawable.ic_calendar_tap);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();
    }

    private void submitData() {
        if(TextUtils.isEmpty(getTilText(R.id.til_fullname))){
            setTilError(R.id.til_fullname,getString(R.string.error_empty_name));
            return;
        }
        if(TextUtils.isEmpty(getTvText(R.id.tv_name_dob))){
            showToast(getString(R.string.error_empty_date_of_birth));
            return;
        }
        if(TextUtils.isEmpty(gender)){
            showToast(getString(R.string.select_gender));
            return;
        }
        if(TextUtils.isEmpty(getTilText(R.id.til_email))) {
            setTilError(R.id.til_email, getString(R.string.error_empty_email));
            return;
        }
        if(!isValidMobile(R.id.til_mobile)){
            return;
        }
        if(spinnerClinic.getSelectedItemPosition()<1){
            showToast(getString(R.string.error_select_clinic));
            return;
        }
        if (profileBitmap != null) {
            uploadImage();
        }else {
            registerReceptionist("");
        }
    }

    private void uploadImage() {
        showProgressDialog();
        FileUploadTask.uploadImage(profileBitmap, new FileUploadTask.OnFileUploadListener() {
            @Override
            public void onSuccess(String imageUrl) {
                profileImageUrl = imageUrl;
                showToast("Image successfully uploaded..!");
                registerReceptionist(profileImageUrl);
            }

            @Override
            public void onFailed() {
                showToast("Image upload failed..!");
                registerReceptionist("");
            }
        }, ReceptionistRegActivity.this);
//        ImageUploadTask imageUploadTask = ImageUploadTask.uploadImage(new ImageUploadTask.UploadListener() {
//            @Override
//            public void onUpload(String imageUrls) {
//                profileImageUrl = imageUrls;
//                hideProgressBar();
//                registerReceptionist(profileImageUrl);
//            }
//
//            @Override
//            public void onUploadFailed() {
//                showToast("Image upload failed");
//                hideProgressBar();
//                registerReceptionist("");
//            }
//        }, profileBitmap);
//        imageUploadTask.execute();
    }

    protected int getHomeIcon() {
        return R.drawable.capture;
    }
    private boolean isValidMobile(int id) {
        String mobile = getTilText(id);
        if (TextUtils.isEmpty(mobile)) {
            setTilError(id, getString(R.string.empty_mobile_error));
            return false;
        }
        if(mobile.length() != 10){
            setTilError(id, getString(R.string.error_invalid_mobile));
            return false;
        }
        return true;
    }

    private void setTilError(int id, String errorInvalidMobile) {
        TextInputLayout viewId = (TextInputLayout) findViewById(id);
        viewId.setError("" + errorInvalidMobile);
    }

    private void registerReceptionist(String imageUrl) {
        String json = getReceptionistData(imageUrl).toString();
        HttpParamObject httpParamObject = RestApiUtil.registerReceptionistRequest(json);
        executeTask(AppConstants.TASK_CODES.REGISTER_RECEPTIONIST, httpParamObject);
    }

    private JSONObject getReceptionistData(String imageUrl) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", getTilText(R.id.til_fullname));
            jsonObject.put("dateOfBirth", getTvText(R.id.tv_name_dob));
            jsonObject.put("gender", gender);
            jsonObject.put("emailId", getTilText(R.id.til_email));
            jsonObject.put("phoneNumber", getTilText(R.id.til_mobile));
            jsonObject.put("imageUrl", imageUrl);
            if(clinicDataList!=null&&clinicDataList.size()>=spinnerClinic.getSelectedItemPosition()-1){
                ClinicData clinicData = clinicDataList.get(spinnerClinic.getSelectedItemPosition() - 1);
                jsonObject.put("clinicId", clinicData.getClinicId());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private String getTvText(int id) {
        TextView viewId = (TextView) findViewById(id);
        if (viewId != null) {
            return viewId.getText().toString();
        }
        return "";
    }

    private String getTilText(int id) {
        TextInputLayout viewId = (TextInputLayout) findViewById(id);
        if (null != viewId) {
            String data = viewId.getEditText().getText().toString();
            if (!TextUtils.isEmpty(data))
                return data;
            else
                viewId.setError("Cant be left empty");
        }
        return "";
    }

    private void unselectAllGenders() {
        tvMale.setTextColor(Color.GRAY);
        tvFemale.setTextColor(Color.GRAY);
    }

    private void setSelect(TextView tv) {
        tv.setTextColor(getResourceColor(R.color.orange));
        gender = tv.getText().toString().toUpperCase();
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response){
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.REGISTER_RECEPTIONIST:
                CreateReceptionistApi createReceptionistApi = (CreateReceptionistApi) response;
                if (null != response && false == createReceptionistApi.getError()) {
                    showToast("Registered successfully!Please login to continue");
                    Bundle bundle=new Bundle();
                    startNextActivity(LoginActivity.class);
                    finish();
                } else {
                    showToast(createReceptionistApi.getMessage());
                    return;
                }
                break;
            case AppConstants.TASK_CODES.GET_ALL_CLINICS:
                ClinicResponse clinicResponse = (ClinicResponse) response;
                if (clinicResponse != null) {
                    List<ClinicData> clinicDataList = clinicResponse.getData();
                    if (!CollectionUtils.isEmpty(clinicDataList)) {
                        getClinicMap(clinicDataList);
                        setCityList();
                    }
                }
                break;

        }
    }
    private void getClinicMap(List<ClinicData> clinicDataList) {
        for (ClinicData clinicData : clinicDataList) {
            Address clinicAddress = clinicData.getClinicAddress();
            if (clinicAddress != null) {
                String city = clinicAddress.getCity();
                if (citiesMap.containsKey(city)) {
                    ArrayList<ClinicData> dataList = citiesMap.get(city);
                    dataList.add(clinicData);
                } else {
                    ArrayList<ClinicData> dataList = new ArrayList<>();
                    dataList.add(clinicData);
                    citiesMap.put(city, dataList);
                }
            }
        }
        listCity.clear();
        listCity.add("---Select city---");
        listCity.addAll(citiesMap.keySet());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
        }
        String s = listClinic.get(position);
        setText(s, android.R.id.text1, convertView);
        return convertView;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        listClinic.clear();
        listClinic.add("---Select clinic---");
        String city = listCity.get(position);
        clinicDataList = citiesMap.get(city);
        if (clinicDataList != null) {
            for (ClinicData clinicData1 : clinicDataList) {
                listClinic.add(clinicData1.getName());
            }
        }
        clinicAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
