package com.example.my.receptionist.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.example.my.receptionist.Model.UploadFileResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.asyncmanager.Service;
import simplifii.framework.asyncmanager.ServiceFactory;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by raghu on 28/1/17.
 */

public class FileUploadTask extends AsyncTask<Bitmap, Void, Boolean> {
    private OnFileUploadListener onFileUploadListener;
    private OnMultiFileUploadListener onMultiFileUploadListener;
    private Context context;
    private boolean ismultipleImages;
    private Bitmap bitmap;
    private List<Bitmap> bitmapList;
    private List<String> imageUrls=new ArrayList<>();
    private boolean isLoaderShows=true;

    private ProgressDialog progressDialog;
    private String imageUrl;

    public static void uploadImage(Bitmap bitmap, OnFileUploadListener onFileUploadListener,Context context) {
        FileUploadTask fileUploadTask = new FileUploadTask();
        fileUploadTask.onFileUploadListener = onFileUploadListener;
        fileUploadTask.ismultipleImages = false;
        fileUploadTask.context=context;
        fileUploadTask.bitmap=bitmap;
        fileUploadTask.execute();
    }
    public static void uploadImage(Bitmap bitmap, OnFileUploadListener onFileUploadListener,Context context,boolean isLoaderShows) {
        FileUploadTask fileUploadTask = new FileUploadTask();
        fileUploadTask.onFileUploadListener = onFileUploadListener;
        fileUploadTask.ismultipleImages = false;
        fileUploadTask.context=context;
        fileUploadTask.bitmap=bitmap;
        fileUploadTask.isLoaderShows=isLoaderShows;
        fileUploadTask.execute();
    }
    public static void uploadMultipleImage(List<Bitmap> bitmapList, OnMultiFileUploadListener onMultiFileUploadListener,Context context) {
        FileUploadTask fileUploadTask = new FileUploadTask();
        fileUploadTask.onMultiFileUploadListener = onMultiFileUploadListener;
        fileUploadTask.ismultipleImages = true;
        fileUploadTask.bitmapList=bitmapList;
        fileUploadTask.context=context;
        fileUploadTask.execute();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(isLoaderShows){
            showProgressBar();
        }
    }

    @Override
    protected Boolean doInBackground(Bitmap[] bitmaps) {
            if (ismultipleImages) {
                for (Bitmap bitmap : bitmapList) {
                    String imageUrl = uploadFile(bitmap);
                    if(!TextUtils.isEmpty(imageUrl)){
                        imageUrls.add(imageUrl);
                    }
                }
                if(imageUrls.size()>0){
                    return true;
                }
            } else {
                imageUrl = uploadFile(bitmap);
                if (!TextUtils.isEmpty(imageUrl)) {
                    return true;
                }
            }
        return false;
    }
    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        hideProgressBar();
        if (result!=null&&result==true) {
            if(ismultipleImages){
                onMultiFileUploadListener.onSuccess(imageUrls);
            }else {
                onFileUploadListener.onSuccess(imageUrl);
            }
        }else {
            if (onFileUploadListener != null) {
                onFileUploadListener.onFailed();
            }
            if (onMultiFileUploadListener != null) {
                onMultiFileUploadListener.onFailed();
            }
        }
    }

    private String uploadFile(Bitmap bitmap) {
        try {
            File file = Util.getFile(bitmap);
            FileParamObject fileParamObject = new FileParamObject(file, file.getName(), "imageFile");
            fileParamObject.setUrl(AppConstants.PAGE_URL.UPLOAD_IMAGE);
            fileParamObject.setClassType(UploadFileResponse.class);
            Service service = ServiceFactory.getInstance(context, AppConstants.TASK_CODES.UPLOAD_IMAGE);
            UploadFileResponse uploadFileResponse = (UploadFileResponse) service.getData(fileParamObject);
            if ((uploadFileResponse != null) && (!uploadFileResponse.getError())) {
                return uploadFileResponse.getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void showProgressBar() {
        progressDialog = ProgressDialog.show(context, "", "Uploading image. Please wait...", true);
    }

    public void hideProgressBar() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog=null;
        }
    }

    public interface OnFileUploadListener {
        void onSuccess(String imageUrl);

        void onFailed();
    }
    public interface OnMultiFileUploadListener {
        void onSuccess(List<String> imageUrlList);

        void onFailed();
    }

}
