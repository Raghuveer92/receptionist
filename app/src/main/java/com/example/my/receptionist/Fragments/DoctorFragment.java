package com.example.my.receptionist.Fragments;

import android.database.DataSetObserver;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.my.receptionist.Model.DoctorHeader;
import com.example.my.receptionist.Model.PatientData;
import com.example.my.receptionist.Model.PatientModel;
import com.example.my.receptionist.Model.PatientQueueData;
import com.example.my.receptionist.Model.PhysicianData;
import com.example.my.receptionist.Model.QueueData;
import com.example.my.receptionist.R;
import com.example.my.receptionist.interfaces.OnRowPopupOptionClicked;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by Neeraj Yadav on 12/22/2016.
 */

public class DoctorFragment extends BaseFragment implements AdapterView.OnItemClickListener {
    private ExpandableAdapter expandableAdapter;
    private List<DoctorHeader> doctorHeaderList = new ArrayList<>();
    private ExpandableListView expandableListView;
    private List<PatientQueueData> patientQueueDataList = new ArrayList<>();
    private OnRowPopupOptionClicked onRowPopupOptionClicked;

    private static DoctorFragment runningInstance;

    public static DoctorFragment getRunningInstance() {
        if(runningInstance==null)
            runningInstance = new DoctorFragment();
        return runningInstance;
    }

    public void setOnRowPopupOptionClicked(OnRowPopupOptionClicked onRowPopupOptionClicked) {
        this.onRowPopupOptionClicked = onRowPopupOptionClicked;
    }

    public void setPatientQueueDataList(List<PatientQueueData> patientQueueDataList) {
        this.patientQueueDataList.clear();
        if(CollectionUtils.isNotEmpty(patientQueueDataList))
            this.patientQueueDataList.addAll(patientQueueDataList);
    }

    @Override
    public void initViews() {
//        setAdapter();
    }

    @Override
    public void refreshData() {

        doctorHeaderList.clear();

        Map<String, List<PatientQueueData>> physicianIdVsQueueEntriesMap = new HashMap<>();
        for(PatientQueueData patientQueueData : patientQueueDataList){
            PhysicianData physicianData = patientQueueData.getPhysicianDetails();
            String physicianId = physicianData.getPhysicianId();

            if(!physicianIdVsQueueEntriesMap.containsKey(physicianId)){
                List<PatientQueueData> patientQueueDataSubLList = new ArrayList<>();
                patientQueueDataSubLList.add(patientQueueData);
                physicianIdVsQueueEntriesMap.put(physicianId, patientQueueDataSubLList);
            } else {
                List<PatientQueueData> patientQueueDataSubLList = physicianIdVsQueueEntriesMap.get(physicianId);
                patientQueueDataSubLList.add(patientQueueData);
            }
        }

        for(Map.Entry<String, List<PatientQueueData>> physicianIdVsQueueEntry : physicianIdVsQueueEntriesMap.entrySet()){
            int patientsInDoctorQueue = physicianIdVsQueueEntry.getValue().size();

            PhysicianData physicianData = physicianIdVsQueueEntry.getValue().get(0).getPhysicianDetails();
            DoctorHeader doctorHeader = new DoctorHeader();
            doctorHeader.setPhysicianId(physicianData.getPhysicianId());
            doctorHeader.setDrName(physicianData.getProfile().getFullname());
            if(CollectionUtils.isNotEmpty(physicianData.getEducation().getSpecializations()))
                doctorHeader.setDrType(physicianData.getEducation().getSpecializations().toString().replace("[","").replace("]",""));
            else
                doctorHeader.setDrType("");
            doctorHeader.setItemCount(String.valueOf(patientsInDoctorQueue));

            SimpleDateFormat uiDateFormatter = new SimpleDateFormat("HH:mm");
            List<PatientModel> patientModels=new ArrayList<>();
            for(PatientQueueData patientQueueData : physicianIdVsQueueEntry.getValue()){
                PatientData patientData = patientQueueData.getPatientDetails();
                QueueData queueData = patientQueueData.getQueueDetails();

                Date date = new Date(queueData.getAppointmentTime());
                String dateInString = uiDateFormatter.format(date);

                PatientModel patientModel=new PatientModel();
                patientModel.setpName(patientData.getName());
                patientModel.setTime(dateInString);
                patientModel.setUserIcon(patientData.getImageUrl());
                patientModel.setQueueId(queueData.getQueueId());
                patientModels.add(patientModel);
            }
            doctorHeader.setPatientModelList(patientModels);
            doctorHeaderList.add(doctorHeader);
        }
        setAdapter();
        expandableAdapter.notifyDataSetChanged();
    }

    private void setAdapter() {
        expandableListView = (ExpandableListView) findView(R.id.exp_lv);
        expandableAdapter = new ExpandableAdapter();
        expandableListView.setAdapter(expandableAdapter);
        expandableListView.setGroupIndicator(null);
        expandableListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    class ExpandableAdapter extends BaseExpandableListAdapter {
        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {
        }


        @Override
        public int getChildrenCount(int groupPosition) {
            return doctorHeaderList.get(groupPosition).getPatientModelList().size();
        }

        @Override
        public int getGroupCount() {
            return doctorHeaderList.size();
        }

        @Override
        public DoctorHeader getGroup(int groupPosition) {
            DoctorHeader doctorHeader = doctorHeaderList.get(groupPosition);
            return doctorHeader;
        }

        @Override
        public PatientModel getChild(int groupPosition, int childPosition) {
            PatientModel patientModel = doctorHeaderList.get(groupPosition).getPatientModelList().get(childPosition);
            return patientModel;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.row_doctor, null, false);
            }
            TextView drName = (TextView) convertView.findViewById(R.id.tv_dr_name);
            TextView drType = (TextView) convertView.findViewById(R.id.tv_dr_spcl);
            TextView count = (TextView) convertView.findViewById(R.id.tv_item_count);
            drName.setText(getGroup(groupPosition).getDrName());
            drType.setText(getGroup(groupPosition).getDrType());
            count.setText(getGroup(groupPosition).getItemCount());
            return convertView;
        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition, final boolean isLastChild, View convertView, final ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.row_item_patient, null, false);
            }
            final List<PatientModel> patientModelss = doctorHeaderList.get(groupPosition).getPatientModelList();
            final PatientModel patientModel = patientModelss.get(childPosition);
            TextView patientName = (TextView) convertView.findViewById(R.id.tv_patient_name);
            TextView time = (TextView) convertView.findViewById(R.id.tv_appointment_time);
            TextView extraName = (TextView) convertView.findViewById(R.id.tv_doctor_name);
            View view = convertView.findViewById(R.id.view_line);
            final ImageView ivOption = (ImageView) convertView.findViewById(R.id.iv_option);

            view.setVisibility(View.GONE);
            extraName.setVisibility(View.GONE);

            final ImageView imageView = (ImageView) convertView.findViewById(R.id.iv_patient_profile_pic);
            patientName.setText(patientModel.getpName());
            time.setText(patientModel.getTime());
            if(!TextUtils.isEmpty(patientModel.getUserIcon()))
                Picasso.with(getActivity()).load(patientModel.getUserIcon()).placeholder(R.drawable.ic_male).into(imageView);
            else{
                imageView.setImageResource(R.drawable.ic_male);
            }

            ivOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(getActivity(), ivOption);
                    popup.getMenuInflater().inflate(R.menu.patient_queue_popup, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()){
                                case R.id.change_physician:
                                    if(onRowPopupOptionClicked!=null){
                                        onRowPopupOptionClicked.changePhysician(patientModel.getQueueId(), doctorHeaderList.get(groupPosition).getPhysicianId());
                                    }
//                                    Toast.makeText(getActivity(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                                    break;
                                case R.id.remove_patient:
//                                    Toast.makeText(getActivity(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                                    if(onRowPopupOptionClicked!=null){
                                        onRowPopupOptionClicked.removePatientQueueFromQueue(patientModel.getQueueId());
                                    }
                                    break;
                            }
                            return true;
                        }
                    });

                    popup.show();
                }
            });


            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

//        @Override
//        public boolean areAllItemsEnabled() {
//            return false;
//        }
//
//        @Override
//        public boolean isEmpty() {
//            return false;
//        }
//
//        @Override
//        public void onGroupExpanded(int groupPosition) {
//
//        }
//
//        @Override
//        public void onGroupCollapsed(int groupPosition) {
//
//        }
//
//        @Override
//        public long g
// etCombinedChildId(long groupId, long childId) {
//            return 0;
//        }
//
//        @Override
//        public long getCombinedGroupId(long groupId) {
//            return 0;
//        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_doctor;
    }
}
