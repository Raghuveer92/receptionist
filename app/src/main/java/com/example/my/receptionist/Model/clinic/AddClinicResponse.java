package com.example.my.receptionist.Model.clinic;

import com.example.my.receptionist.Model.BaseApiResponse;

/**
 * Created by raghu on 16/1/17.
 */

public class AddClinicResponse extends BaseApiResponse {
    ClinicData data;

    public ClinicData getData() {
        return data;
    }
}
