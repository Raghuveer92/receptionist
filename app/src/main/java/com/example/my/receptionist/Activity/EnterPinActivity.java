package com.example.my.receptionist.Activity;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.my.receptionist.Model.GetAuthKeyApi;
import com.example.my.receptionist.Model.OTPData;
import com.example.my.receptionist.Model.PatientData;
import com.example.my.receptionist.Model.SendOtpResponse;
import com.example.my.receptionist.R;
import com.example.my.receptionist.utils.RestApiUtil;
import com.squareup.picasso.Picasso;

public class EnterPinActivity extends BaseActivity implements TextWatcher{

    TextView tvPatientName;
    EditText editTextPin;
    LinearLayout linearLayout;
    ImageView ivPatientPic;
    private PatientData patientData;
    private String otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_pin);
        initToolBar("Patient Found");
        linearLayout = (LinearLayout) findViewById(R.id.ll_dotbar);
        editTextPin = (EditText)findViewById(R.id.et_pin);
        tvPatientName = (TextView) findViewById(R.id.tv_patient_name);
        ivPatientPic = (ImageView) findViewById(R.id.iv_patient_pic);
        setPatientName();
        editTextPin.addTextChangedListener(this);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        sendOtp();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response==null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.SEND_OTP:
                SendOtpResponse sendOtpResponse= (SendOtpResponse) response;
                if(sendOtpResponse!=null){
                    if(sendOtpResponse.getError()){
                        showToast(sendOtpResponse.getMessage());
                        return;
                    }
                    OTPData otpData = sendOtpResponse.getData();
                    if(otpData!=null){
                        otp = otpData.getOneTimePassword();
                        if(otpData.isDebugMode()){
                            showToast("Debug mode is on, your pin is "+ otp);
                        }
                    }}
                break;
        }
    }

    private void sendOtp() {
        if (patientData != null) {
            HttpParamObject httpParamObject = RestApiUtil.generateAndSendOtp(patientData.getPhoneNumber());
            executeTask(AppConstants.TASK_CODES.SEND_OTP, httpParamObject);
        }
    }

    private void setPatientName() {
        if(getIntent()!=null){
            patientData = (PatientData)getIntent().getSerializableExtra(AppConstants.BUNDLE_KEYS.PATIENT_DATA);
            if(patientData!=null){
                tvPatientName.setText(patientData.getName());
                if (!TextUtils.isEmpty(patientData.getImageUrl()))
                    Picasso.with(EnterPinActivity.this).load(patientData.getImageUrl()).placeholder(R.drawable.ic_male_orange).into(ivPatientPic);
                else
                    ivPatientPic.setImageResource(R.drawable.ic_male);
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(s.length()==1){
            linearLayout.setVisibility(View.VISIBLE);
        }
        if(s.length()==4){
            linearLayout.setVisibility(View.GONE);
            validatePin(editTextPin.getText().toString());
        }
    }

    private void validatePin(String pin){
        if(pin.equals(String.valueOf(otp))){
            Intent intent = new Intent(EnterPinActivity.this, DetailPatientFoundActivity.class);
            intent.putExtras(getIntent());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else{
            showToast(getString(R.string.invalid_pin));
            return;
        }
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.back_arrow;
    }
}
