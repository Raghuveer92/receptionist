package com.example.my.receptionist.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dhillon on 05-10-2016.
 */

public class PhysicianData {

    @SerializedName("physicianId")
    @Expose
    private String physicianId;
    @SerializedName("registrationNumber")
    @Expose
    private String registrationNumber;
    @SerializedName("profile")
    @Expose
    private Profile profile;
    @SerializedName("education")
    @Expose
    private Education education;
    @SerializedName("certificates")
    @Expose
    private Certificates certificates;

    private boolean isSelected;

    public String getPhysicianId() {
        return physicianId;
    }

    public void setPhysicianId(String physicianId) {
        this.physicianId = physicianId;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public Certificates getCertificates() {
        return certificates;
    }

    public void setCertificates(Certificates certificates) {
        this.certificates = certificates;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}