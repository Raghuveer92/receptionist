package com.example.my.receptionist.Activity;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.receptionist.Model.ReceptionistData;
import com.example.my.receptionist.R;

public class HomeActivity extends BaseActivity {

    ImageView ivAddPatientToQueue, ivRegisterPatient, ivSeeQueue, ivLogout;
    TextView tvAddPatientToQueue, tvRegisterPatient, tvSeeQueue, tvLogout;
    private ReceptionistData receptionistData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        receptionistData = ReceptionistData.getInstance();
        if (receptionistData == null)
            showToast("Unable to fetch data");
        else {
            initToolBar(receptionistData.getName());
            getSupportActionBar().setSubtitle("Receptionist, " + receptionistData.getClinicName());
        }

        tvAddPatientToQueue = (TextView) findViewById(R.id.tv_add_pat_queue);
        tvRegisterPatient = (TextView) findViewById(R.id.tv_reg_patient);
        tvSeeQueue = (TextView) findViewById(R.id.tv_see_queue);
        tvLogout = (TextView) findViewById(R.id.tv_logout);

        ivAddPatientToQueue = (ImageView) findViewById(R.id.iv_add_pat_queue);
        ivRegisterPatient = (ImageView) findViewById(R.id.iv_reg_patient);
        ivSeeQueue = (ImageView) findViewById(R.id.iv_see_queue);
        ivLogout = (ImageView) findViewById(R.id.iv_logout);

        setOnClickListener(R.id.lay_add_pat_queue, R.id.lay_reg_patient, R.id.lay_see_queue, R.id.lay_logout);
    }

    @Override
    protected void onHomePressed() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUnselectAll();
    }

    @Override
    public void onClick(View v) {
        setUnselectAll();
        switch (v.getId()) {
            case R.id.lay_add_pat_queue:
                setSelect(tvAddPatientToQueue, ivAddPatientToQueue);
                startNextActivity(ValidatePatientActivity.class);
                break;
            case R.id.lay_reg_patient:
                setSelect(tvRegisterPatient, ivRegisterPatient);
                Intent intent = new Intent(HomeActivity.this, PatientRegistrationActivity.class);
                intent.putExtra(AppConstants.REDIRECT_TO, HomeActivity.class.getSimpleName());
                startActivity(intent);
                break;
            case R.id.lay_see_queue:
                setSelect(tvSeeQueue, ivSeeQueue);
                startNextActivity(PatientQueueActivity.class);
                break;
            case R.id.lay_logout:
                setSelect(tvLogout, ivLogout);
                Preferences.saveData(AppConstants.PREF_KEYS.IS_LOGIN, false);
                Preferences.removeData(AppConstants.PREF_KEYS.RECEPTIONIST_DATA);
                startNextActivity(SplashActivity.class);
                finish();
                break;
        }
        super.onClick(v);
    }

    private void setUnselectAll() {
        tvAddPatientToQueue.setTextColor(getResourceColor(R.color.color_primary));
        tvRegisterPatient.setTextColor(getResourceColor(R.color.color_primary));
        tvSeeQueue.setTextColor(getResourceColor(R.color.color_primary));
        tvLogout.setTextColor(getResourceColor(R.color.color_primary));
        ivAddPatientToQueue.setImageResource(R.drawable.ic_add_patient_violet);
        ivRegisterPatient.setImageResource(R.drawable.ic_new_patient_violet);
        ivSeeQueue.setImageResource(R.drawable.ic_patient_queue_violet);
        ivLogout.setImageResource(R.drawable.ic_settings_violet);
    }

    private void setSelect(TextView textView,ImageView imageView) {
        textView.setTextColor(getResourceColor(R.color.orange));
        switch (imageView.getId()){
            case R.id.iv_add_pat_queue:
                imageView.setImageResource(R.drawable.ic_add_patient);
                return;
            case R.id.iv_reg_patient:
                imageView.setImageResource(R.drawable.ic_new_patient_orange);
                return;
            case R.id.iv_see_queue:
                imageView.setImageResource(R.drawable.ic_patient_queue_orange);
                return;
            case R.id.iv_logout:
                ivLogout.setImageResource(R.drawable.ic_settings_orange);
                return;
        }
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.capture;
    }
}
