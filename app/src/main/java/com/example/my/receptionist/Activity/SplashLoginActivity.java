package com.example.my.receptionist.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.my.receptionist.R;

import simplifii.framework.activity.BaseActivity;

public class SplashLoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_login);
        setOnClickListener(R.id.btn_login, R.id.btn_register);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                startNextActivity(LoginActivity.class);
                break;
            case R.id.btn_register:
                startNextActivity(ReceptionistRegActivity.class);
                break;
        }
    }
}
